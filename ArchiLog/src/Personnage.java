public abstract class Personnage {
    private static int next_id = 0;
    private int id;
    private int vie;
    private int pos_x;
    private int pos_y;
    private int defense;
    private int attaque;
    private int vitesse;
    private Personnage focus;
    private int equipe;
    private int portee;
    private int type;

    protected Personnage(int vie, int defense, int attaque, int vitesse, int equipe, int portee, int t){
        this.vie = vie;
        this.defense = defense;
        this.attaque = attaque;
        this.vitesse = vitesse;
        this.equipe = equipe;
        this.portee = portee;
        id = next_id;
        next_id++;
        type = t;
        focus = null;
    }

    public int getType(){
        return type;
    }

    public void setType(int t) {
        type = t;
    }
    
    public int getId(){
        return id;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getPos_x() {
        return pos_x;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getAttaque() {
        return attaque;
    }

    public void setAttaque(int attaque) {
        this.attaque = attaque;
    }

    public int getVitesse() {
        return vitesse;
    }

    public void setVitesse(int vitesse) {
        this.vitesse = vitesse;
    }

    public Personnage getFocus() {
        return focus;
    }

    public void setFocus(Personnage focus) {
        this.focus = focus;
    }

    public int getEquipe() {
        return equipe;
    }

    public void setEquipe(int equipe) {
        this.equipe = equipe;
    }

    public int getPortee() {
        return portee;
    }

    public void setPortee(int portee) {
        this.portee = portee;
    }

    void attaquer(){ //attaque la cible
        if(estAPortee()){
            focus.setVie(focus.getVie() - (attaque - focus.getDefense()));
            if(focus.getVie()<0){ //si la cible meurt
                Carte.nbUnites[focus.getEquipe()-1] --;
                Carte.terrains[focus.getPos_x()][focus.getPos_y()].setPerso(null);
                focus = null;
            }
        }
    }

    boolean estAPortee(){ //indique si la cible est a portee
        boolean atteignable = false;
        if(focus != null) {
            int i = -portee;
            while (i <= portee && !atteignable) {
                int j = -portee;
                while (focus.getPos_x() == getPos_x() + i && j <= portee && !atteignable) {
                    if (focus.getPos_y() == getPos_y() + j) {
                        atteignable = true;
                    }
                    j++;
                }
                i++;
            }
        }
        return atteignable;
    }

    abstract void seDeplacer(); //permet d'effectuer un deplacement
    abstract void choixCible(); //permet de determiner sa cible


}
