import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ZoneGraphique extends Canvas {
	Carte carte;
	static int tailleCase=40;
	
	public ZoneGraphique() {
	}
	public ZoneGraphique(Carte pCarte) {
		super(Carte.nb_ligne*tailleCase, Carte.nb_colonne*tailleCase);
		setGrille(pCarte);
		
	}
	public void setGrille(Carte c) {
		carte = c;
	}
	
	public void paint(){
		GraphicsContext g = getGraphicsContext2D();
		for(int i=0; i<carte.nb_ligne;i++){
			for(int j=0; j<carte.nb_colonne; j++) {
				g.fillRect(tailleCase*i, tailleCase*j, tailleCase, tailleCase);
				g.setFill(Parametres.CouleurTerrains[carte.terrains[i][j].getType()-1]);
				if(carte.terrains[i][j].getPerso() != null) {
					Image img = new Image("/ressources/"+carte.terrains[i][j].getPerso().getType()+carte.terrains[i][j].getPerso().getEquipe()+".png");	
					g.drawImage(img, tailleCase*i, tailleCase*j);
				}
			}
		}
	}
	
	
}
