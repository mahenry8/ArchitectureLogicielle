import javafx.scene.paint.Color;

public class Parametres {
	static int nb_type_terrain = 10;
	static int nb_equipe = 3;
	static int nb_type_unite = 4;	//1: archer, 2: guerrier, 3: mage, 4: cavalier
	
	static Color CouleurEquipes[]= {Color.RED,Color.BLUE, Color.PINK};
	static Color CouleurTerrains[]= {Color.BURLYWOOD, 	// 1 : chateau
									Color.GOLD, 		// 2 : desert
									Color.DARKGREEN,	// 3 : foret
									Color.DARKKHAKI,	// 4 : marais
									Color.GRAY,			// 5 : montagne 
									Color.LIGHTGRAY,	// 6 : muraille
									Color.BLANCHEDALMOND, // 7 : plage
									Color.CHARTREUSE, 	// 8 : plaine
									Color.CORNFLOWERBLUE, // 9 : riviere
									Color.ROSYBROWN		// 10 : village							
									};
	}
