public class Mage extends Personnage{
    public Mage(int equipe){ super(60,4, 20, 3, equipe, 4,3);}

    public void choixCible() { //attaque l'ennemi avec le plus d'attaque
        for(int i = -getPortee(); i <= getPortee(); i++){
            for(int j = -getPortee(); j<=getPortee();j++){
                if(getPos_x()+i>=0 && getPos_y()+j>=0 && getPos_x()+i<Carte.terrains.length && getPos_y()+j<Carte.terrains[0].length
                        && Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso() != null
                        && Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso().getEquipe() != getEquipe()){
                    if(getFocus() == null){
                        setFocus(Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso());
                    }
                    else if(getFocus().getAttaque()< Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso().getAttaque()){
                        setFocus(Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso());
                    }
                }
            }
        }

    }

    public void seDeplacer(){ //s'eloigne de sa cible et reste a portee ou cherche un terrain avec un meilleur bonus de defense
        Carte.terrains[getPos_x()][getPos_y()].depart();
        if(getFocus()!=null) {
            int dep = getVitesse();
            while (dep > 0) {
                if (getFocus().getPos_x() > getPos_x() && getPos_x()>0 && Carte.terrains[getPos_x()-1][getPos_y()].getPerso() == null) {
                    setPos_x(getPos_x()-1);
                    if(!estAPortee()) setPos_x(getPos_x()+1);
                }
                else if(getFocus().getPos_x()< getPos_x() && getPos_x()<Carte.terrains.length-1 && Carte.terrains[getPos_x()+1][getPos_y()].getPerso() == null){
                    setPos_x(getPos_x()+1);
                    if(!estAPortee()) setPos_x(getPos_x()-1);
                }
                else if(getFocus().getPos_y()>getPos_y() && getPos_y()>0 && Carte.terrains[getPos_x()][getPos_y()-1].getPerso() == null){
                    setPos_y(getPos_y()-1);
                    if(!estAPortee()) setPos_y(getPos_y()+1);
                }
                else if(getFocus().getPos_y()>getPos_y() && getPos_y()<Carte.terrains[0].length-1 && Carte.terrains[getPos_x()][getPos_y()+1].getPerso() == null){
                    setPos_y(getPos_y()+1);
                    if(!estAPortee()) setPos_y(getPos_y()-1);
                }
                dep--;
            }
        }else{
            int x = getPos_x();
            int y = getPos_y();
            for(int i = -getVitesse(); i <= getVitesse(); i++) {
                if(getPos_x() + i >= 0 && getPos_x() + i < Carte.terrains.length) {
                    for (int j = -getVitesse(); j <= getVitesse(); j++) {
                        if (getPos_y() + j >= 0 && getPos_y() + j < Carte.terrains[0].length && Carte.terrains[getPos_x() + i][getPos_y() + j].getDefBonus()>= Carte.terrains[x][y].getDefBonus()){
                            x = getPos_x() + i;
                            y = getPos_y() + j;
                        }
                    }
                }
            }
            setPos_x(x);
            setPos_y(y);
        }
        Carte.terrains[getPos_x()][getPos_y()].arrivee(this);
    }
}
