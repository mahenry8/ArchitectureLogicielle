public abstract class Terrain {
    private Personnage perso;
    private int attBonus;
    private int defBonus;
    private int type;

    protected Terrain(int a, int d, int t){
        attBonus = a;
        defBonus = d;
        type =t;
        perso = null;
    }

    public int getAttBonus() {
        return attBonus;
    }

    public int getDefBonus() {
        return defBonus;
    }

    public Personnage getPerso() {
        return perso;
    }

    public int getType() {
        return type;
    }

    public void setPerso(Personnage perso) {
        this.perso = perso;
    }

    public void setType(int t) {
    	type =t;
    }

    public void arrivee(Personnage p){ //ajoute les bonus au personnage arrivant
        if(perso == null && p != null) {
            p.setAttaque(p.getAttaque() + getAttBonus());
            p.setDefense(p.getDefense() + getDefBonus());
            setPerso(p);
        }
    }

    public void depart(){
        if(perso != null) { //retire les bonus au personnage s'en allant
            getPerso().setDefense(getPerso().getDefense() - getDefBonus());
            getPerso().setAttaque(getPerso().getAttaque() - getAttBonus());
            setPerso(null);
        }
    }
}
