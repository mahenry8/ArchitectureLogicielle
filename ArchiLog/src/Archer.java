public class Archer extends Personnage {

    public Archer(int equipe) {
        super(40,4, 16, 4, equipe, 5,1);
    }

    public void choixCible() {//attaque l'ennemi le plus proche
        boolean cible = false;
        int p = 1;
        while (!cible && p <= getPortee()) {
            int i = -p;
            while (!cible && i <= p) {
                int j = -p;
                while (!cible && j <= p) {
                    if (i + getPos_x() >= 0 && j + getPos_y() >= 0 && i + getPos_x() < Carte.terrains.length && j + getPos_y() < Carte.terrains[0].length &&
                            Carte.terrains[getPos_x() + i][getPos_y() + j].getPerso() != null &&
                            Carte.terrains[i + getPos_x()][j + getPos_y()].getPerso().getEquipe() != getEquipe()) {
                        setFocus(Carte.terrains[getPos_x() + i][getPos_y() + j].getPerso());
                        cible = true;
                    }
                    j++;
                }
                i++;
            }
            p++;
        }
    }


    public void seDeplacer(){ //s'eloigne de sa cible ou cherche la meilleure position offensive
        Carte.terrains[getPos_x()][getPos_y()].depart();
        if(getFocus()!=null) {
            int dep = getVitesse();
            while (dep > 0) {
                if (getFocus().getPos_x() > getPos_x() && getPos_x()>0 && Carte.terrains[getPos_x()-1][getPos_y()].getPerso() == null) {
                    setPos_x(getPos_x()-1);
                }
                else if(getFocus().getPos_x()< getPos_x() && getPos_x()<Carte.terrains.length-1 && Carte.terrains[getPos_x()+1][getPos_y()].getPerso() == null){
                    setPos_x(getPos_x()+1);
                }
                else if(getFocus().getPos_y()>getPos_y() && getPos_y()>0 && Carte.terrains[getPos_x()][getPos_y()-1].getPerso() == null){
                    setPos_y(getPos_y()-1);
                }
                else if(getFocus().getPos_y()>getPos_y() && getPos_y()<Carte.terrains[0].length-1 && Carte.terrains[getPos_x()][getPos_y()+1].getPerso() == null){
                    setPos_y(getPos_y()+1);
                }
                dep--;
            }
        }else{
            int x = getPos_x();
            int y = getPos_y();
            for(int i = -getVitesse(); i <= getVitesse(); i++) {
                if(getPos_x() + i >= 0 && getPos_x() + i < Carte.terrains.length) {
                    for (int j = -getVitesse(); j <= getVitesse(); j++) {
                        if (getPos_y() + j >= 0 && getPos_y() + j < Carte.terrains[0].length && Carte.terrains[getPos_x() + i][getPos_y() + j].getAttBonus()>= Carte.terrains[x][y].getAttBonus()){
                            x = getPos_x() + i;
                            y = getPos_y() + j;
                        }
                    }
                }
            }
            setPos_x(x);
            setPos_y(y);
        }
        Carte.terrains[getPos_x()][getPos_y()].arrivee(this);
    }
}
