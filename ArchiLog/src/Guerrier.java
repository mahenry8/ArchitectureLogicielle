import java.util.Random;

public class Guerrier extends Personnage {
    public Guerrier(int equipe) { super(100,6, 20, 2, equipe, 1,2); }

    public void choixCible(){ //attaque l'ennemi a portee avec le plus de point de vie
        for(int i = -getPortee(); i <= getPortee(); i++){
            for(int j = -getPortee(); j<=getPortee();j++){
                if(getPos_x()+i>=0 && getPos_y()+j>=0 && getPos_x()+i<Carte.terrains.length && getPos_y()+j<Carte.terrains[0].length
                        && Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso() != null
                        && Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso().getEquipe() != getEquipe()){
                    if(getFocus() == null){
                        setFocus(Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso());
                    }
                    else if(getFocus().getVie()< Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso().getVie()){
                        setFocus(Carte.terrains[getPos_x()+i][getPos_y()+j].getPerso());
                    }
                }
            }
        }
    }

    public void seDeplacer(){ //se rapproche de sa cible ou avance tout droit aleatoirement
        int dep = getVitesse();
        Carte.terrains[getPos_x()][getPos_y()].depart();
        if (getFocus() != null) {
            while (dep > 0) {
                if (getFocus().getPos_x() > getPos_x() && getPos_x() < Carte.terrains.length - 1 && Carte.terrains[getPos_x() + 1][getPos_y()].getPerso() == null) {
                    setPos_x(getPos_x() + 1);
                } else if (getFocus().getPos_x() < getPos_x() && getPos_x() > 0&& Carte.terrains[getPos_x() - 1][getPos_y()].getPerso() == null) {
                    setPos_x(getPos_x() - 1);
                } else if (getFocus().getPos_y() > getPos_y() && getPos_y() < Carte.terrains[0].length - 1 && Carte.terrains[getPos_x()][getPos_y() + 1].getPerso() == null) {
                    setPos_y(getPos_y() + 1);
                } else if (getFocus().getPos_y() < getPos_y() && getPos_y() > 0 && Carte.terrains[getPos_x()][getPos_y() - 1].getPerso() == null) {
                    setPos_y(getPos_y() - 1);
                }
                dep--;
            }
        }else{
            Random random = new Random();
            int dir = random.nextInt(4);
            switch (dir) {
                case 0:
                    while (dep>0) {
                        if (getPos_x() + 1 < Carte.terrains.length && Carte.terrains[getPos_x() + 1][getPos_y()].getPerso() == null) setPos_x(getPos_x() + 1);
                        dep --;
                    }
                    break;
                case 1:
                    while (dep>0) {
                        if (getPos_x() - 1 >= 0 && Carte.terrains[getPos_x() - 1][getPos_y()].getPerso() == null) setPos_x(getPos_x() - 1);
                        dep --;
                    }
                    break;
                case 2:
                    while (dep>0) {
                        if (getPos_y() + 1 < Carte.terrains[0].length && Carte.terrains[getPos_x()][getPos_y()+1].getPerso() == null) setPos_y(getPos_y() + 1);
                        dep --;
                    }
                    break;
                case 3:
                    while (dep>0) {
                        if(getPos_y() -1 >= 0 && Carte.terrains[getPos_x()][getPos_y()-1].getPerso() == null)setPos_y(getPos_y() - 1);
                        dep --;
                    }
                    break;
                default:
                    break;
            }
        }
        Carte.terrains[getPos_x()][getPos_y()].arrivee(this);
    }
}
