import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.util.Duration;


public class SimulationBataille extends Application {
	Carte carte;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private void jouerTour() { //effectue un tour de jeu
		for(int i = 0; i < Carte.nb_ligne; i++ ){
			for(int j = 0; j < Carte.nb_colonne; j++){
				if(Carte.terrains[i][j].getPerso() !=null){
					Carte.terrains[i][j].getPerso().choixCible();
					Carte.terrains[i][j].getPerso().attaquer();
					Carte.terrains[i][j].getPerso().choixCible();
					Carte.terrains[i][j].getPerso().seDeplacer();
				}
			}
		}

	}
	
	@Override
	public void start (Stage primaryStage) {
		primaryStage.setTitle("Simulateur de Batailles");
		carte = new Carte(20,20);
		//carte.toString();
		BorderPane root = new BorderPane();
		
		/*
		 * Affichage de la carte
		 */
		ZoneGraphique aff = new ZoneGraphique(carte);
		aff.paint();
		root.setCenter(aff);
		
		/* 
		 * Tours de jeu 
		 * */
		Timeline timeline = new Timeline(new KeyFrame(
		        Duration.millis(1000),
		        ae -> { jouerTour(); aff.paint();}));
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
		
		/*
		 * Affichage des legendes
		 */
		Image img = new Image("/ressources/gauche.png");
		ImageView iv = new ImageView();
		iv.setImage(img);
		root.setLeft(iv);
		
		img = new Image("/ressources/droite.png");
		iv = new ImageView();
		iv.setImage(img);
		root.setRight(iv);
		
		img = new Image("/ressources/bottom.png");
		iv = new ImageView();
		iv.setImage(img);
		root.setBottom(iv);
		
		
		primaryStage.setScene(new Scene(root,1900,1000));
		primaryStage.setFullScreen(true);
		primaryStage.show();
	}
}
