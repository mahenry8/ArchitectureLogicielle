import java.util.Random;

public class Carte {
	static public Terrain [][] terrains; //matrice representant le champ de bataille
	static public int nb_colonne;
	static public int nb_ligne;
	static public int[] nbUnites; //nombre d'unites de chaque faction
	
	public Carte(int nbc,int nbl) {
		nbUnites = new int[Parametres.nb_equipe]; //on initialise le nombre d'unites de chaque faction
		for(int i = 0; i < Parametres.nb_equipe; i++) {
			nbUnites[i] = 0;
		}
		nb_colonne = nbc;
		nb_ligne = nbl;
		terrains = new Terrain[nb_ligne][nb_colonne];
		for(int i=0; i< nb_colonne;i++){ //on cree les terrains aleatoirement
			for(int j=0; j<nb_ligne;j++){
				Random rd = new Random();
				switch(rd.nextInt(Parametres.nb_type_terrain)+1) {
				case 1:		//chateau
					terrains[i][j]= new Chateau();
					break;
				case 2:		//Desert
					terrains[i][j]= new Desert();
					break;
				case 3:		//Foret
					terrains[i][j]= new Foret();
					break;
				case 4:		//Marais
					terrains[i][j]= new Marais();
					break;
				case 5:		//Montagne
					terrains[i][j]= new Montagne();
					break;
				case 6:		//Muraille
					terrains[i][j]= new Muraille();
					break;
				case 7:		//Plage
					terrains[i][j]= new Plage();
					break;
				case 8:		//Plaine
					terrains[i][j]= new Plaine();
					break;
				case 9:		//Riviere
					terrains[i][j]= new Rivere();
					break;
				case 10:		//Village
					terrains[i][j]= new Village();
					break;
				}
			}
		}
		for(int i=0; i< nb_colonne;i++){ //on fait arriver les personnages aleatoirement sur des cases
			for(int j=0; j<nb_ligne;j++){
				if(Math.random()<0.3)
					terrains[i][j].setPerso(null);
				else {
					Random rd = new Random();
					switch(rd.nextInt(Parametres.nb_type_unite)+1) {
					case 1:		//archer
						Archer a = new Archer(rd.nextInt((Parametres.nb_equipe))+1);
						a.setPos_x(i);
						a.setPos_y(j);
						terrains[i][j].arrivee(a);
						nbUnites[a.getEquipe()-1] ++;
						break;
					case 2 :	//guerrier
						Guerrier g = new Guerrier(rd.nextInt((Parametres.nb_equipe))+1);
						g.setPos_x(i);
						g.setPos_y(j);
						terrains[i][j].arrivee(g);
						nbUnites[g.getEquipe()-1] ++;
						break;
					case 3:		//Mage
						Mage m = new Mage(rd.nextInt((Parametres.nb_equipe))+1);
						m.setPos_x(i);
						m.setPos_y(j);
						terrains[i][j].arrivee(m);
						nbUnites[m.getEquipe()-1] ++;
						break;
					case 4: 	//cavalier
						Cavalier c = new Cavalier(rd.nextInt((Parametres.nb_equipe))+1);
						c.setPos_x(i);
						c.setPos_y(j);
						terrains[i][j].arrivee(c);
						nbUnites[c.getEquipe()-1] ++;
						break;
					}
				}
			}
		}
		
	}

	@Override
	public String toString() { //permet d'afficher le terrain sur le terminal
		String result ="";
		for(int i=0; i< nb_colonne;i++) {
			for(int j=0; j<nb_ligne;j++) {
				if(terrains[i][j].getPerso() != null) {
					result += terrains[i][j].getPerso().getType()+" ";
					//result += terrains[i][j].getType()+" ";
				}
				else
					result += "x ";
			}
				
			result += "\n";
		}
		return result;
	}
}
